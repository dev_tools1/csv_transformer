class Header:
    def __init__(self, source, target, logic):
        self.target = target
        self.source = source
        self.logic = logic


class CsvFile:
    def __init__(self, headers, name, batch_size=None):
        self.headers = headers
        self.name = name
        self.batch_size = batch_size


class CellLogic:
    def __init__(self, statement, logic):
        self.statement = statement
        self.logic = logic

    def execute(self, data):
        return self.logic

class Statement:
    def __init__(self, source_header, sign, condition):
        self.source_header = source_header
        self.sign = sign
        self.condition = condition

    def is_true(self, data_source, row_id):
        return True