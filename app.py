# 4 add basic logic if | gt |lt |eq |gte|lte| or|and|not|replace|arithemtic|str upper|lower|ucfirst
# 6 add database
# 7 add queueing of csv generation
# 8 optimize generation
import pandas as pd
from flask import request, render_template
from flask import Flask
from jinja2 import Template
from project_enum import LogicConditionEnum

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def home():
    return render_template('upload_csv.html')


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        uploaded_file = request.files['file']
        if uploaded_file.filename != '':
            df = pd.read_csv(request.files['the_file'])
            return df.columns.values

    return render_template('upload_csv.html')


@app.route('/map', methods=['GET', 'POST'])
def map_headers():
    if request.method == 'POST':
        for target_value in request.values:
            print('Target Header is %s and Source header %s' % (target_value, request.form.get(target_value)))
            print('####')

    template_source = open('./templates/map_headers.html').read()
    jinja_template = Template(template_source)
    target = pd.read_csv('target.csv')
    source = pd.read_csv('source.csv')

    return jinja_template.render({
        'target_columns': target.columns.values,
        'source_columns': source.columns.values,
    })

@app.route('/logic', methods=['GET', 'POST'])
def add_logic():
    if request.method == 'POST':
        for target_value in request.values:
            print('Target Header is %s and Source header %s' % (target_value, request.form.get(target_value)))
            print('####')

    template_source = open('./templates/add_logic.html').read()
    jinja_template = Template(template_source)
    target = pd.read_csv('target.csv')
    source = pd.read_csv('source.csv')

    return jinja_template.render({
        'target_columns': target.columns.values,
        'source_columns': source.columns.values,
        'signs': [a.value for a in LogicConditionEnum]
    })