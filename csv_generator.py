from dto import CsvFile, CellLogic


class CsvGenerator:
    def generate_csv(self, csv_file_dto: CsvFile, source_data):
        new_csv = []
        for header in csv_file_dto.headers:
            data = source_data[header.source]

            for row in data:
                logic: CellLogic
                for logic in header.logic:
                    if logic.statement.is_true():
                        row = logic.execute(row)

                new_csv[header.target] = row

        return new_csv
