import enum


class LogicConditionEnum(enum.Enum):
    gt = 1
    lw = 2
    eq = 3
    contains = 4
